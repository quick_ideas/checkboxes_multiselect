package com.michalrys.checkboxmultiselect.sample;

import com.sun.javafx.stage.StageHelper;
import javafx.scene.control.CheckBox;
import javafx.stage.Stage;

import java.awt.*;
import java.util.List;

public class WindowController implements WindowMVC.Controller {
    private final WindowMVC.View view;
    private List<CheckBox> checkBoxesXandY;
    private List<CheckBox> checkBoxesYonly;
    private Stage currentStage;
    private Point pointMousePressed;
    private Point pointMouseReleased;

    public WindowController(WindowMVC.View view) {
        this.view = view;
    }

    @Override
    public void setCheckboxesXandY(List<CheckBox> checkBoxes) {
        this.checkBoxesXandY = checkBoxes;
    }

    @Override
    public void setCheckboxesYonly(List<CheckBox> checkBoxesYonly) {
        this.checkBoxesYonly = checkBoxesYonly;
    }

    @Override
    public void paneMousePressed() {
        currentStage = StageHelper.getStages().get(0);
        pointMousePressed = setPointMouse();
        view.setLabelStatus("Status: mouse pressed.");
    }

    @Override
    public void paneMouseReleased() {
        pointMouseReleased = setPointMouse();
        view.setLabelStatus("Status: mouse released.");

        for(CheckBox cb : checkBoxesXandY) {
            if (isCheckBoxBetweenMousePressedReleasedOnXandY(cb)) {
                switchSelectionOfCheckBox(cb);
            }
        }

        for(CheckBox cb : checkBoxesYonly) {
            if (isCheckBoxBetweenMousePressedReleasedOnYOnly(cb)) {
                switchSelectionOfCheckBox(cb);
            }
        }
    }

    private Point setPointMouse() {
        Point point = MouseInfo.getPointerInfo().getLocation();
        double xMouse = point.getX();
        double yMouse = point.getY();

        double xStage = currentStage.getX();
        double yStage = currentStage.getY();

        int xBias = 8; //TODO i do not know why there is some offeset on X
        int yBias = 31; //TODO i do not know why there is some offeset on y

        Point pointMouseToSet = new Point();
        pointMouseToSet.setLocation(xMouse - xStage - xBias, yMouse - yStage - yBias);
        return pointMouseToSet;
    }

    private boolean isCheckBoxBetweenMousePressedReleasedOnXandY(CheckBox cb) {
        boolean isOnX = (
                pointMousePressed.getX() > cb.getLayoutX() + cb.getHeight())
                && (pointMouseReleased.getX() < cb.getLayoutX()
        ) || (
                (pointMousePressed.getX() < cb.getLayoutX())
                        && (pointMouseReleased.getX() > cb.getLayoutX() + cb.getHeight()));

        boolean isOnY = (
                pointMousePressed.getY() > cb.getLayoutY() + cb.getHeight())
                && (pointMouseReleased.getY() < cb.getLayoutY()
        ) || (
                (pointMousePressed.getY() < cb.getLayoutY())
                        && (pointMouseReleased.getY() > cb.getLayoutY() + cb.getHeight()));

        return isOnX && isOnY;
    }

    private boolean isCheckBoxBetweenMousePressedReleasedOnYOnly(CheckBox cb) {
        boolean isOnY = (
                pointMousePressed.getY() > cb.getLayoutY() + cb.getHeight())
                && (pointMouseReleased.getY() < cb.getLayoutY()
        ) || (
                (pointMousePressed.getY() < cb.getLayoutY())
                        && (pointMouseReleased.getY() > cb.getLayoutY() + cb.getHeight()));

        return isOnY;
    }
    private void switchSelectionOfCheckBox(CheckBox cb) {
        if (cb.isSelected()) {
            cb.setSelected(false);
        } else {
            cb.setSelected(true);
        }
    }


}
