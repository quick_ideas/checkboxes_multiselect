package com.michalrys.checkboxmultiselect.sample;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;

import java.util.ArrayList;
import java.util.List;

public class WindowView implements WindowMVC.View {
    private WindowMVC.Controller controller = new WindowController(this);
    private List<CheckBox> checkBoxesXandY = new ArrayList<>();
    private List<CheckBox> checkBoxesYonly = new ArrayList<>();

    @FXML
    private Label labelStatus;

    @FXML
    private CheckBox cb1;

    @FXML
    private CheckBox cb2;

    @FXML
    private CheckBox cb3;

    @FXML
    private CheckBox cb4;

    @FXML
    private CheckBox cb5;

    @FXML
    private CheckBox cb6;

    @FXML
    private CheckBox cb7;

    @FXML
    private CheckBox cb8;

    @FXML
    void initialize() {
        checkBoxesXandY.add(cb1);
        checkBoxesXandY.add(cb2);
        checkBoxesXandY.add(cb3);
        checkBoxesXandY.add(cb4);
        checkBoxesYonly.add(cb5);
        checkBoxesYonly.add(cb6);
        checkBoxesYonly.add(cb7);
        checkBoxesYonly.add(cb8);
        controller.setCheckboxesXandY(checkBoxesXandY);
        controller.setCheckboxesYonly(checkBoxesYonly);
    }

    @FXML
    void paneMousePressed() {
        controller.paneMousePressed();
    }

    @FXML
    void paneMouseReleased() {
        controller.paneMouseReleased();
    }

    @Override
    public void setLabelStatus(String newStatus) {
        labelStatus.setText(newStatus);
    }
}
