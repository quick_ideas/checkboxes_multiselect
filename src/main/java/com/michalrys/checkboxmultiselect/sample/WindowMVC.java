package com.michalrys.checkboxmultiselect.sample;

import javafx.scene.control.CheckBox;

import java.util.List;

public interface WindowMVC {
    interface Controller {
        void paneMousePressed();

        void paneMouseReleased();

        void setCheckboxesXandY(List<CheckBox> checkBoxes);

        void setCheckboxesYonly(List<CheckBox> checkBoxesYonly);
    }

    interface View {
        void setLabelStatus(String newStatus);
    }
}
